from dataclasses import dataclass
from functools import singledispatchmethod
from uuid import UUID
from .model import Account
from ..shared import Repository

@dataclass(frozen=True)
class OpenAccount:
    id: UUID
    name: str

@dataclass(frozen=True)
class TransferFunds:
    from_acc: UUID
    from_acc_version: int
    to_acc: UUID
    to_acc_version: int
    amount: float

class AccountService:
    def __init__(self, repo: Repository):
        self._repo = repo

    @singledispatchmethod
    def handle(self, command): pass

    @handle.register
    def _(self, command: OpenAccount):
        acc = Account.open(command.id, command.name)
        self._repo.save(acc, -1)

    @handle.register
    def _(self, command: TransferFunds):
        from_acc = self._repo.get(command.from_acc)
        to_acc = self._repo.get(command.to_acc)
        from_acc.debit(command.amount)
        to_acc.credit(command.amount)
        self._repo.save(from_acc, command.from_acc_version)
        self._repo.save(to_acc, command.to_acc_version)
