from dataclasses import dataclass
from functools import singledispatchmethod
from uuid import UUID
from .model import AccountOpened, AccountBalanceChanged

@dataclass
class AccountListItemDto:
    id: UUID
    version: int
    name: str
    balance: float

class AccountListView:
    def __init__(self, db):
        self._db = db
        self.__handles__ = (AccountOpened, AccountBalanceChanged)

    @singledispatchmethod
    def handle(self, event):
        raise NotImplementedError()

    @handle.register
    def _(self, event: AccountOpened):
        e = event
        self._db.list_add(AccountListItemDto(e.id, e.version, e.name, 0.0))

    @handle.register
    def _(self, event: AccountBalanceChanged):
        item = self._db.list_get(event.id)
        item.balance += event.amount
        item.version = event.version
        self._db.list_update(item)

_bull_shit_database = list()

class AccountsDatabase:
    def list_add(self, item):
        _bull_shit_database.append(item)

    def list_get(self, id):
        return next(filter(lambda i: i.id == id, _bull_shit_database))

    def list_update(self, item):
        idx = _bull_shit_database.index(self.list_get(item.id))
        _bull_shit_database[idx] = item

class AccountsReadModel:
    def get_accounts(self):
        return tuple(_bull_shit_database)
