from dataclasses import dataclass
from uuid import UUID
from ..shared import AggregateRoot

@dataclass(frozen=True)
class AccountOpened:
    id: UUID
    name: str

@dataclass(frozen=True)
class AccountBalanceChanged:
    id: UUID
    amount: float

class Account(AggregateRoot):
    @classmethod
    def open(cls, id, name):
        obj = cls()
        obj.apply_change(AccountOpened(id, name))
        return obj

    @AggregateRoot.apply.register
    def _(self, event: AccountOpened):
        self.id = event.id

    def debit(self, amount):
        self.apply_change(AccountBalanceChanged(self.id, -amount))

    def credit(self, amount):
        self.apply_change(AccountBalanceChanged(self.id, amount))
