import logging
from abc import ABC, abstractmethod
from collections import defaultdict
from functools import reduce, singledispatchmethod
from typing import Callable
from uuid import UUID

class Bus:
    def __init__(self):
        self._handlers = defaultdict(list)

    def register(self, handler, messages):
        for message in messages:
            self._handlers[message].append(handler)

    def publish(self, message):
        '''Publish a message to all subscribers.'''
        for handler in self._get_handlers(message):
            handler(message)

    def send(self, message):
        '''Sends a message to a subscriber.

        RuntimeError is raised if 0 or more than 1 subscribers exists.
        '''
        handlers = self._get_handlers(message)
        if len(handlers) == 0:
            raise RuntimeError('No handler.', type(message))
        elif len(handlers) > 1:
            raise RuntimeError('More than one handler.', type(message))
        else:
            handlers[0](message)

    def _get_handlers(self, message):
        return self._handlers[type(message)]

class AggregateVersionMismatch(RuntimeError): pass
class AggregateDoesNotExist(RuntimeError): pass

class EventStore:
    def __init__(self, bus):
        self._bus = bus
        self._event_descriptors = dict()

    def save_events(self, id: UUID, events, expected_version: int):
        version = expected_version

        # try get event descriptors, otherwise create empty list
        stored = self._event_descriptors.get(id, [])
        if len(stored) == 0:
            self._event_descriptors[id] = stored

        # ensure current version is as expected
        elif stored[-1].version != version and version != -1:
            raise AggregateVersionMismatch()

        # iterate through events, increasing the version with each one processed
        for event in events:
            version += 1
            event.__dict__['version'] = version
            stored.append(event)
            self._bus.publish(event)

    def get_events(self, id: UUID):
        try:
            return tuple(self._event_descriptors[id])
        except KeyError:
            raise AggregateDoesNotExist()

class AggregateRoot(ABC):
    def __init__(self):
        self._changes = []

    @property
    def uncommitted(self):
        return tuple(self._changes)

    def commit(self):
        self._changes.clear()

    @singledispatchmethod
    def apply(self, event): pass

    def load_from_history(self, history):
        for event in history:
            self.apply_change(event, _new=False)

    def apply_change(self, event, _new=True):
        self.apply(event)
        if _new:
            self._changes.append(event)

class Repository:
    def __init__(self, factory: Callable[[], AggregateRoot], store: EventStore):
        self.instance_factory = factory
        self._store = store

    def get(self, id: UUID):
        obj = self.instance_factory()
        history = self._store.get_events(id)
        obj.load_from_history(history)
        return obj

    def save(self, item: AggregateRoot, expected_version: int):
        self._store.save_events(item.id, item.uncommitted, expected_version)
