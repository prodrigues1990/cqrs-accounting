import logging
from uuid import UUID, uuid4

from src.domain.shared import Bus, Repository
from src.domain import accounts as acc

from src.data.flat_file import EventStore, AccountsReadDb

data_dir = '.data'

cmd_bus = Bus()
evt_bus = Bus()
store = EventStore(evt_bus, data_dir)

acc_repo = Repository(acc.Account, store)
acc_svc = acc.AccountGateway(acc_repo)
cmd_bus.register(acc_svc.handle, (acc.OpenAccount, acc.TransferFunds))

acc_db = AccountsReadDb(data_dir)
acc_list = acc.AccountListView(acc_db)
evt_bus.register(acc_list.handle, (acc.AccountOpened, acc.AccountBalanceChanged))

def selecionar_conta(prompt, contas):
    prompt = f'{prompt} (c para cancelar) '
    while True:
        for i, conta in enumerate(contas):
            print(f'{i}) {conta_viewmodel(conta)}')
        idx = input(prompt)
        try:
            if idx == 'c': return
            return contas[int(idx)]
        except Exception:
            print('conta inválida')
            return

def indicar_quantidade(prompt):
    prompt = f'{prompt} (c para cancelar) '
    while True:
        idx = input(prompt)
        if idx == 'c': return
        try:
            return float(idx)
        except Exception:
            print('quantidade inválida')
            return

def conta_viewmodel(conta):
    return f'{conta.name} {conta.balance:.2f}€'

def main():
    bus = cmd_bus
    while True:
        message = input('> ').lower().split(' ')
        command = next(iter(message), '')
        args = message[1:]

        if command == 'q':
            break

        try:
            if command == 'ac':                         # abrir conta
                bus.send(acc.OpenAccount(uuid4(), *args))
            elif command == 'tf':                       # transferir fundos
                contas = acc_db.get_accounts()
                conta_1 = selecionar_conta('conta a debitar: ', contas)
                if conta_1 is None: continue
                conta_2 = selecionar_conta('conta a creditar: ', contas)
                if conta_2 is None: continue
                quantidade = indicar_quantidade('quantidade: ')
                if quantidade is None: continue
                bus.send(acc.TransferFunds(
                    conta_1.id,
                    conta_1.version,
                    conta_2.id,
                    conta_2.version,
                    quantidade,
                ))
            elif command == 'lc':                       # listar contas
                for conta in acc_db.get_accounts():
                    print(conta_viewmodel(conta))
            else:
                print('''\
        ac -> Abrir Conta: ac nome_conta
        tf -> Transferir Fundos: tf
        lc -> Listar Contas
        q  -> Sair
    ''')
        except RuntimeError as err:
            logging.exception(err)
            print('err', err)
