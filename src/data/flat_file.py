import pickle
import os

from src.domain.shared import AggregateVersionMismatch, AggregateDoesNotExist

class EventStore:
    def __init__(self, bus, data_dir):
        self._bus = bus
        self._dir = data_dir

        if not os.path.isdir(data_dir):
            os.makedirs(data_dir)

    def save_events(self, id, events, expected_version):
        version = expected_version

        # try get event descriptors, otherwise create empty list
        stored = self._get(id)

        # ensure current version is as expected
        if len(stored) > 0:
            if stored[-1].version != version and version != -1:
                raise AggregateVersionMismatch()

        # iterate through events, increasing the version with each one processed
        for event in events:
            version += 1
            event.__dict__['version'] = version
            stored.append(event)
            self._store(id, stored)
            self._bus.publish(event)

    def get_events(self, id):
        events = self._get(id)
        if len(events) == 0:
            raise AggregateDoesNotExist()
        return tuple(events)

    def _get(self, id):
        if not os.path.isfile(f'{self._dir}/{id}'):
            return []
        with open(f'{self._dir}/{id}', 'rb') as file:
            return pickle.load(file)

    def _store(self, id, events):
        with open(f'{self._dir}/{id}', 'wb') as file:
            pickle.dump(events, file)

class AccountsReadDb:
    def __init__(self, data_dir):
        self._file = f'{data_dir}/account_list'

        if not os.path.isdir(data_dir):
            os.makedirs(data_dir)

        if not os.path.isfile(self._file):
            self._items = []
            with open(self._file, 'wb') as f:
                pickle.dump(self._items, f)
        else:
            with open(self._file, 'rb') as f:
                self._items = pickle.load(f)

    def list_get(self, id):
        return next(filter(lambda i: i.id == id, self._items))

    def list_add(self, item):
        self._items.append(item)
        with open(self._file, 'wb') as f:
            pickle.dump(self._items, f)

    def list_update(self, item):
        for i, _item in enumerate(self._items):
            if _item.id == item.id:
                self._items[i] = item
        with open(self._file, 'wb') as f:
            pickle.dump(self._items, f)

    def get_accounts(self):
        return tuple(self._items)
