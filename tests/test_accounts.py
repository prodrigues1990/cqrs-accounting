from unittest import TestCase
from uuid import uuid4
from src.domain.shared import Repository, EventStore, Bus
from src.domain.accounts import *

class TestOpenAccount(TestCase):
    def setUp(self):
        bus = Bus()
        store = EventStore(bus)
        repo = Repository(Account, store)
        self.svc = AccountGateway(repo)
        bus.register(self.handle, (AccountOpened, AccountBalanceChanged))
        self.events = []

    def test_openAccount(self):
        cmd = OpenAccount(uuid4(), 'account name')
        self.svc.handle(cmd)
        expect = (AccountOpened(cmd.id, cmd.name),)
        self.assertEventsEqual(expect)

    def handle(self, event):
        self.events.append(event)

    def assertEventsEqual(self, expected):
        self.assertEqual(tuple(self.events), expected)
